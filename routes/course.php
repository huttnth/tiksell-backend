<?php

use App\Http\Controllers\Tiksell\CourseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register.js API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'course','middleware' => ['check.auth']], function () {
    Route::get('/list', [CourseController::class, "listCourse"])->name('course.list');
    Route::post('/detail', [CourseController::class, "detailCourse"])->name('course.detail');
});

