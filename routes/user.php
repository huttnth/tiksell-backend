<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\Tiksell\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register.js API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user','middleware' => ['auth:api','user.status']], function () {
    Route::post('/create', [UserController::class, "addNew"])->name('blog.create');
    Route::post('/update', [UserController::class, "update"])->name('blog.update');
    Route::post('/detail', [UserController::class, "detail"])->name('blog.detail');
    Route::post('/status', [UserController::class, "changeStatus"])->name('blog.delete');
    Route::post('/list', [UserController::class, "list"])->name('blog.list');
});

