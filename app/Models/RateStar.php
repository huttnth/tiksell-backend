<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RateStar extends Model
{
    use HasFactory;

    protected $table = 'rate_stars';

    protected $fillable = [
        "start1",
        "start2",
        "start3",
        "start4",
        "start5",
        "point_average",
        "course_id",
    ];

    public function course(): HasOne
    {
        return $this->hasOne(Course::class, "id", "course_id");
    }
}
