<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CategoryItem extends Model
{
    use HasFactory;

    protected $table = 'category_item';

    protected $fillable = [
        "parent_id",
        "name",
        "path_name",
        "status",
    ];
    public function category(): HasOne
    {
        return $this->hasOne(Category::class, "id", "parent_id");
    }
}
