<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Category extends Model
{
    use HasFactory;

    public $timestamps = true;
    protected $table = "categories";

    protected $fillable = [
        "course_id",
        "name",
        "status",
    ];

    public function category_items(): HasMany
    {
        return $this->hasMany(CategoryItem::class, "parent_id", "id")->where("status","!=",config('constants.course.status.lock'));
    }

    public function course(): HasOne
    {
        return $this->hasOne(Course::class, "id", "course_id")->where("status","!=",config('constants.course.status.lock'));
    }


}
