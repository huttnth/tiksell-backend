<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course extends Model
{
    use HasFactory;

    protected $table = 'courses';

    protected $fillable = [
        "title",
        "price",
        "rate_number",
        "count_number",
        "description",
        "status",
    ];


    public function users(): BelongsToMany
    {
        return $this->BelongsToMany(Course::class, "user_course", 'user_id', 'course_id');
    }

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class, "course_id", "id")->where("status","!=",config('constants.course.status.lock'));
    }

}
