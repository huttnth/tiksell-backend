<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';

    protected $fillable = [
        "transaction_code",
        "receipt_id",
        "status",
    ];

    public function receipt(): HasOne
    {
        return $this->hasOne(Receipt::class, "id", "receipt_id");
    }
}
