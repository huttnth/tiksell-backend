<?php

namespace App\Http\Response;

trait Response
{
    function responseResult($message,$data)
    {
        return [
            "data" => $data ?? "",
            "message" => $message ?? ""
        ];
    }
}
