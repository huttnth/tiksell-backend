<?php

namespace App\Http\Repository;

use App\Http\Iterface\ICategoryRepository;
use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements ICategoryRepository
{

    /**
     * @param array $data
     */
    function batchInsertCategory(array $data)
    {
        return Category::insert($data);
    }

    /**
     * @param int $courseId
     * @param array $status
     * @return Collection
     */
    function getCategoriesByCourseId(int $courseId, array $status): array
    {
        $categories = Category::where("course_id",$courseId)->whereIn("status",$status)->get();
        return $categories;
    }
}
