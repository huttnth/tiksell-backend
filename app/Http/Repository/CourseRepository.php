<?php

namespace App\Http\Repository;

use App\Http\Iterface\ICourseRepository;
use App\Models\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class CourseRepository implements ICourseRepository
{

    /**
     * @param array $data
     * @return Course
     */
    public function createCourse(array $data): Course
    {
        $course = Course::create([$data]);
        return $course;
    }

    /**
     * @param int $id
     * @param array $status
     * @return Course
     */
    public function detailCourse(int $id, array $status): Course
    {
        $course = Course::whereId($id)
            ->whereIn("status", $status)
            ->with(["categories"=>function($query){
                return $query->with(["category_items"]);
            }])
            ->first();
        return $course;
    }

    /**
     * @param array $status
     */
    public function listCourse(array $status)
    {
        $courses = Course::whereIn("status", $status)
            ->with("categories")
            ->get();
        return $courses;
    }

    /**
     * @param int $id
     * @param array $data
     * @param array $status
     * @return bool
     */
    public function updateCourse(int $id, array $data, array $status): bool
    {
        $course = Course::whereId($id)
            ->whereIn("status", $status)
            ->first()->update([
                $data
            ]);
        if (!$course) return false;
        return true;
    }

    /**
     * @param int $userId
     * @param int $courseId
     */
    public function getCoursesOfUser(int $userId, int $courseId)
    {
        $coursesUser =  DB::table('user_course')
            ->where("user_id",$userId)
            ->where("course_id",$courseId)
            ->first();
        return $coursesUser;
    }
}
