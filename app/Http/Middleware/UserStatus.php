<?php

namespace App\Http\Middleware;

use App\Http\Response\AuthenticationResponse;
use Closure;
use Illuminate\Http\Request;

class UserStatus
{
    use AuthenticationResponse;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|string[]
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$user = auth("api")->user()) {
            return $this->responseResult("Người dùng không được tìm thấy", null);
        }
        if (
            isset($user->status) ? ($user->status == config('constants.user.status.lock')) : true
        ) {
            return $this->responseResult("Người dùng không được tìm thấy", null);
        }
        return $next($request);
    }
}
