<?php

namespace App\Http\Iterface;

interface ICategoryRepository
{
    /**
     * @param array $data
     */
    function batchInsertCategory(array $data);

    /**
     * @param int $courseId
     * @param array $status
     * @return array
     */
    function getCategoriesByCourseId(int $courseId, array $status): array;
}
