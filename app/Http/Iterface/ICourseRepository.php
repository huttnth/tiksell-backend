<?php

namespace App\Http\Iterface;

use App\Models\Course;

interface ICourseRepository
{

    /**
     * @param array $data
     * @return Course
     */
    public function createCourse(array $data): Course;

    /**
     * @param int $id
     * @param array $status
     * @return Course
     */
    public function detailCourse(int $id, array $status): Course;


    /**
     * @param array $status
     */
    public function listCourse(array $status);


    /**
     * @param int $id
     * @param array $status
     * @param array $data
     * @return bool
     */
    public function updateCourse(int $id, array $data,array $status): bool;

    /**
     * @param int $userId
     * @param int $courseId
     */
    public function getCoursesOfUser(int $userId, int $courseId);
}
