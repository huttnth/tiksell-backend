<?php

namespace App\Http\Controllers\Tiksell;

use App\Http\Controllers\Controller;
use App\Http\Repository\CategoryRepository;
use App\Http\Repository\CourseRepository;
use App\Http\Response\Response;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception as SQLiteException;

class CourseController extends Controller
{
    use Response;

    /**
     * @var CourseRepository
     */
    private CourseRepository $courseRepository;

    /**
     * @var Repository|Application
     */
    private  $statusActive;

    /**
     * @var Repository|Application
     */
    private $statusLock;

    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;

    public function __construct(
        CourseRepository   $courseRepository,
        CategoryRepository $categoryRepository
    )
    {

        $this->courseRepository = $courseRepository;
        $this->statusActive = config('constants.course.status.active');
        $this->statusLock = config('constants.course.status.lock');
        $this->categoryRepository = $categoryRepository;
    }

    function addNew(Request $request): array
    {
        $data = [
            "title" => $request->title ?? "",
            "description" => $request->description ?? "",
            "price" => $request->price > 0 ? $request->price : 0,
            "rate_number" => $request->rate_number > 0 ? $request->rate_number : 0,
            "count_number" => $request->count_number > 0 ? $request->count_number : 0,
            "status" => $this->statusActive,
            "created_at" => Carbon::now()
        ];
        try {
            $course = $this->courseRepository->createCourse($data);
        } catch (SQLiteException $exception) {
            Log::error('Create Course Fail: ' . $exception->getMessage(), $exception->getTrace());
            return $this->responseResult(
                "Tạo khóa học thất bại!",
                null
            );
        }
        return $this->responseResult(
            "Tạo khóa học thành công!",
            $course
        );
    }

    function detailCourse(Request $request): array
    {
        $isActiveCourse = false;
        try {
            $user = auth()->user();
            if (!is_null($user)) {
                $user = $user->with(["receipts"])->first();
                if (!is_null($user->receipts)) {
                    $courseIds = $user->receipts->pluck("course_id")->toArray();
                    $courseUser = $this->courseRepository->getCoursesOfUser($user->id, $request->id);
                    if (!is_null($courseUser) && in_array($request->id, $courseIds)) {
                        $isActiveCourse = true;
                    }
                }
            }
            $course = $this->courseRepository->detailCourse($request->id, [$this->statusActive]);
            if (!is_null($course->categories)) {
                $course->categories->map(function ($category) use ($isActiveCourse) {
                    if ($category->category_items) {
                        $category->category_items = $category->category_items->map(function ($item)
                        use ($isActiveCourse) {
                            if ($isActiveCourse) {
                                return $item;
                            }
                            $item->path_name = "";
                            return $item;
                        });
                    }
                    return $category;
                });
            }


        } catch (SQLiteException $exception) {
            Log::error('Detail Course Fail: ' . $exception->getMessage(), $exception->getTrace());
            return $this->responseResult(
                "Không tìm thấy khóa học",
                null
            );
        }
        return $this->responseResult(
            "",
            $course
        );
    }

    function deleteCourse(Request $request): array
    {
        try {
            $course = $this->courseRepository->updateCourse(
                $request->id,
                ["status" => $this->statusLock],
                [$this->statusActive]);
        } catch (SQLiteException $exception) {
            Log::error('Delete Course Fail: ' . $exception->getMessage(), $exception->getTrace());
            return $this->responseResult(
                "Xóa khóa học thất bại ",
                false
            );
        }
        return $this->responseResult(
            "",
            $course
        );
    }

    function updateCourse(Request $request): array
    {
        try {
            $course = $this->courseRepository->updateCourse(
                $request->id,
                $request->all(),
                [$this->statusActive]);
        } catch (SQLiteException $exception) {
            Log::error('Updatee Course Fail: ' . $exception->getMessage(), $exception->getTrace());
            return $this->responseResult(
                "Cập nhật khóa học thất bại ",
                false
            );
        }
        return $this->responseResult(
            "",
            $course
        );
    }

    function listCourse(): array
    {
        try {
            $courses = $this->courseRepository->listCourse([$this->statusActive]);
        } catch (SQLiteException $exception) {
            Log::error('List Course Fail: ' . $exception->getMessage(), $exception->getTrace());
            return $this->responseResult(
                "không có khóa học nào",
                []
            );
        }
        return $this->responseResult(
            "",
            $courses
        );
    }
}
